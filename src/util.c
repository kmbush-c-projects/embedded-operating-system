#include "ucode.c"


/**************LOGIN HELPER FUNCTIONS**************************/

//helper function to splice a string by the ':'
//this is used to parse the passwd file
int splice(int startIndex, char *buf, char *line)
{
	int i = 0;
	while (line[startIndex] != ':')
	{
		buf[i++] = line[startIndex++];
	}
	buf[i] = '\0';
	return startIndex + 1;
}

//helper function that gets the userdata at a current place in the passwd file
void getUserData(char *curName, char *curPassword, int *uid, int *gid, char *line)
{
	char suid[64], sgid[64];
	int startIndex = 0;

	//get name, passwd, uid, gid
	startIndex = splice(startIndex, curName, line);		
	startIndex = splice(startIndex, curPassword, line);	
	startIndex = splice(startIndex, suid, line);			
	splice(startIndex, sgid, line);						

	*uid = atoi(suid);	//convert string representation of uid to int
	*gid = atoi(sgid);	//convert string representation of gid to int
}


/**************************************************************/
/**************************************************************/
/**************************************************************/
/*********************GREP HELPER FUNCTIONS********************/

char * search(char *string, char *substring)
{
	//printf("in the search function from util.c");
	char *a, *b;
	b = substring;
	if (*b == 0) {
		return string;
	}
	for (; *string != 0; string += 1) {
		if (*string != *b) {
			continue;
		}
		a = string;
		while (1) {
			if (*b == 0) {
				return string;
			}
			if (*a++ != *b++) {
				break;
			}
		}
		b = substring;
	}
	return (char *)0;
}

/**************************************************************/
/**************************************************************/
/**************************************************************/
/*******************LS HELPER FUNCTIONS***********************/

//This is a utility function that lists information for a file
//it starts with two strings representing the permission bits from ls -l in linux
//also checks for links and handles accordingly
void fileInfo(char *filePath)
{
	//permission bits
	char *t1 = "xwrxwrxwr-------";
	char *t2 = "----------------";
	
	STAT F;
	char buf[1024];
	int i;
	getBasename(buf, filePath);

	if (stat(filePath, &F) == 0)
	{
		//writing if its dir, reg, or link...
		if ((F.st_mode & 0xF000) == 0x8000) { printf("%c", '-'); }
		if ((F.st_mode & 0xF000) == 0x4000) { printf("%c", 'd'); }
		if ((F.st_mode & 0xF000) == 0xA000) { printf("%c", 'l'); }

		//writing the permission bits...
		for (i = 8; i >= 0; i--)
		{
			if (F.st_mode & (1 << i)) { printf("%c", t1[i]); }
			else { printf("%c", t2[i]); }
		}
		printf("  ");
		
		//prints the actual information of the file
		printf("%3d", F.st_nlink);
		printf("%3d", F.st_gid);
		printf("%3d", F.st_uid);
		printf("%6d", F.st_size);
		printf("%s", buf);


		if ((F.st_mode & 0xF000) == 0xA000)
		{
			if (readlink(filePath, buf) != -1)
			{
				printf(" -> %s", buf);
			}
			else
			{
				dup2(1, 4); dup2(2, 1); printf("ls: %s: could not read link\n", filePath); dup2(4, 1);
			}
		}
		printf("\n");
	}
	else
	{
		dup2(1, 4); dup2(2, 1); printf("ls: %s: does not exist\n", filePath); dup2(4, 1);
	}
}


/**************************************************************/
/**************************************************************/
/**************************************************************/
/*********************L2U HELPERS******************************/

//simple helper... just converts lowercase to uppercase
void toUpper(char *str)
{
	int i = 0;
	while (str[i])
	{
		/* check if current char is lowercase: */
		if (str[i] > 96 && str[i] < 123)
			str[i] -= 32;
		i++;
	}
}

/**********************************************/
