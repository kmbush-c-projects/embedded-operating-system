/********** ls.c file *************/
#include "util.c"

int main(int argc, char *argv[ ])
{
	int i = 1;
	STAT F;
	char cwd[1024];

	//no filename, we ls from cwd, otherwise we have a path to a directory to ls
	if (argc == 1)
	{
		getcwd(cwd);
		//strcpy(argv[1], cwd);
		argv[1] = cwd;
		argv[2] = 0;
		argc++;
	}

	//begin the ls process
	while (argv[i] != 0)
	{
		if (stat(argv[i], &F) == 0)
		{
			if (argc > 2)
			{
				if (i != 1)
				{
					printf("\n");
				}
				printf("%s:\n", argv[i]);
			}
			
			/* the file is directory so ls everything inside of it: */
			if ((F.st_mode & 0xF000) == 0x4000)
			{
				DIR *dp;
				char *cp;
				int j;
				char buf[BLKSIZE];
				char tmp[BLKSIZE];
				char curName[BLKSIZE];
				int fd;
				int size = F.st_size;

				fd = open(argv[i], O_RDONLY);
				while (size > 0)
				{
					read(fd, buf, BLKSIZE);
					size -= BLKSIZE;
					
					dp = (DIR *)buf;
					cp = buf;

					while (cp < buf + BLKSIZE)
					{
						for (j = 0; j < dp->name_len; j++)
						{
							curName[j] = dp->name[j];
						}
						curName[dp->name_len] = '\0';		//must add null terminator to end because there isnt one

						strcpy(tmp, argv[i]);
						strcpy(tmp + strlen(tmp), "/");
						strcpy(tmp + strlen(tmp), curName);

						fileInfo(tmp);

						cp += dp->rec_len; // advance cp to the next entry (by rec_len)
						dp = (DIR *)cp;	//have the dir entry follow along
					}
				}
			}

			/* the file is just a file so ls it directly: */
			else
			{
				fileInfo(argv[i]);
			}
		}

		//directory we are trying to ls does not exist
		else
		{
			dup2(1, 4); dup2(2, 1); printf("ls: %s: does not exist\n", argv[i]); dup2(4, 1);
		}
		i++;
	}
}
