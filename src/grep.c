/********** grep.c file *************/
#include "util.c"

int main(int argc, char *argv[ ])
{
	int i = 2, in;
	
	char line[256], c;
	
	//incorrect parameters
	if (argc == 1)
	{
		dup2(1, 4); dup2(2, 1);
		printf("grep: missing pattern operand\n");		//write to stderr that no manual page was included
		printf("Try 'man grep' for more information.\n");
		dup2(4, 1);
	}
	
	//correct parameters for grep
	else
	{
		//no filename, reading from stdin until control-D
		if (argc == 2)
		{
			while (get2s(line) != 0)
			{
				if (search(line, argv[1]) != 0)
				{
					printf("%s", line);
				}
			}
		}
		
		//more than one argument... means we have to grep more than one file
		else
		{
			//for all files...
			while (argv[i] != 0)
			{
				//read from file
				in = open(argv[i], O_RDONLY);		

				//verify file exists
				if (in < 0)
				{
					dup2(1, 4); dup2(2, 1); printf("cat: %s: could not open file\n", argv[i]); dup2(4, 1);
				}

				//file exists, execute grep
				else
				{
					dup2(in, 0);
					while (get2s(line) != 0)
					{
						//grab the line you want and print it
						if (search(line, argv[1]) != 0)
						{
							printf("%s", line);
						}
					}
					close(in);
				}
				i++;
			}
		}
	}
}
